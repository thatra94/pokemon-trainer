# PokemonTrainer

Built with angular

To run progran first open terminal in the server folder then type:

- npm Start

Then open terminal in client folder and type

- ng serve

Application should now be visbile on localhost:4200

## Pokemon API

- [x] Use the Pokemon API to display Pokemon with their avatars. https://pokeapi.co

## Database

- [x] make use of the Json-Server as a "Database solution". Should store trainer name and their "caught" Pokemon

## Landing Page

- [x] Able to input their "Trainer name"
- [x] Store trainer name in the database solution
- [x] When users returns to the web app, they must not be asked for their trainer name again, buut simply redirected to the Pokemon page.

## Trainer Page

- [x] Must display all the Pokemon the Trainer has collected
- [x] Should display a list of Pokemon with their avatar.
- [x] Each list item must be clickable and take the user to the Pokemon Detail page for the specific Pokemon that was clicked.

## Pokemon Catalogue

- [x] Users may not have acccess to this page unless they have entered a trainer name.
- [x] The catalogue must be a list of "card style" Pokemon presented to the user.
- [x] The Pokemon should have an image(sprite) and name displayed,
- [x] Each Pokemon card must be clickable and take the user to the Pokemon detail page

## Pokmeon Detail

- [x] Display an image of the Pokemon along with its abilities, stats, height, weight and types.
- [x] Button to "Collect" this Pokemon for the logged in Trainer.
- [x] Should store collected Pokemon locally and used to display the collected Pokemon in the Trainer page

**Base Stats**

- [x] Image
- [x] Types
- [x] Base Stats
- [x] Name

**Profile**

- [x] Height
- [x] Weight
- [x] Abilities
- [x] Base Experience

**Moves**

- [x] A list of moves(Just the name will do)

## Minimum Requirements

- [x] Use the latest Angular with the Angular CLI

Use Components to

- [x] Create "Root" or "Parent" components for pages
- [x] Create reusable pieces of UI

MUST use the Angular Router

- [x] To selectively disokay "parent" components based on the URL's path

Use the Angular Guard pattern

- [x] To Restrict access to pages

Use Services to:

- [x] Share data between components
- [x] Make HTTP Requests using the HttpClient

Group your components logically

- [ ] Make use of the FacadePattern (Optional but HIGHLY recommended)

Use Guards to stop users from accessing pages that require a login

- [x] Pokemon catalogue page
- [x] Pokemon Detail page
- [x] Profile/Trainer page
