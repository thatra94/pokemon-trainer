import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session.service';
import { setStorage } from 'src/app/utils/storage.utils';
import { AppRoutes } from '../../enums/app-routes.enum';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent {
  constructor(
    private readonly sessionService: SessionService,
    private readonly router: Router
  ) {}

  get hasActiveSession(): boolean {
    return this.sessionService.active();
  }

  logout(): void {
    localStorage.clear();
    this.router.navigateByUrl(AppRoutes.LOGIN);
  }
}
