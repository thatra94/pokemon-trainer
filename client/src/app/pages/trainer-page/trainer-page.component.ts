import { Component, OnInit } from '@angular/core';
import { TrainerService } from 'src/app/services/trainer.service';
import { Pokemon } from 'src/app/models/pokemon.model';
import { Pagination } from 'src/app/utils/pagination.util';

@Component({
  selector: 'app-trainer-page',
  templateUrl: './trainer-page.component.html',
  styleUrls: ['./trainer-page.component.css'],
})
export class TrainerPageComponent implements OnInit {
  error: string = '';

  constructor(private readonly trainerService: TrainerService) {}

  ngOnInit(): void {
    this.trainerService.fetchUserPokemon();
  }
  get pokemon(): Pokemon[] {

    return this.trainerService.pokemon;
  }
}
