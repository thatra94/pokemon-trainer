import { Component } from '@angular/core';

@Component({
  selector: 'app-not-found-container',
  template: ` <div class="container">404 Not found</div> `,
  // Inline styles in Angular
  styles: [
    `
      .container {
        max-width: 1200px;
        width: 100%;
        margin: 0 auto;
        padding: 0 1em;
      }
    `,
  ],
})
export class NotFoundContainerComponent {}
