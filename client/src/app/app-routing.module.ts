import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SessionGuard } from './guards/session.guard';
import { CataloguePageComponent } from './pages/catalogue-page/catalogue-page.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { NotFoundContainerComponent } from './pages/not-found-page/not-found-page.component';
import { PokemonDetailPageComponent } from './pages/pokemon-detail-page/pokemon-detail-page.component';
import { TrainerPageComponent } from './pages/trainer-page/trainer-page.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'login' },

  { path: 'login', component: LoginPageComponent },
  {
    path: 'trainer',
    component: TrainerPageComponent,
    canActivate: [SessionGuard],
  },
  {
    path: 'pokemon',
    component: CataloguePageComponent,
    canActivate: [SessionGuard],
  },
  {
    path: 'pokemon/:name',
    component: PokemonDetailPageComponent,
    canActivate: [SessionGuard],
  },
  { path: '**', component: NotFoundContainerComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
