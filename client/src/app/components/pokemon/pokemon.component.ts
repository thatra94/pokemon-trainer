import { Component, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.css'],
})
export class PokemonComponent {
  @Input() pokemon;
  @Output() clicked: EventEmitter<number> = new EventEmitter();

  onPokemonClicked(): void {
    this.clicked.emit(this.pokemon.id);
  }
}
