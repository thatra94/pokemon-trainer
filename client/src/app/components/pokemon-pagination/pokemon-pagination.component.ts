import { Component, Input } from '@angular/core';
import { PokemonService } from 'src/app/services/pokemon.service';
import { Pagination } from 'src/app/utils/pagination.util';

@Component({
  selector: 'app-pokemon-pagination',
  templateUrl: './pokemon-pagination.component.html',
  styleUrls: ['./pokemon-pagination.component.css'],
})
export class PokemonPaginationComponent {
  constructor(private readonly pokemonService: PokemonService) {}
  get isFirstPage(): boolean {
    return this.pokemonService.isFirstPage;
  }

  get isLastPage(): boolean {
    return this.pokemonService.isLastPage;
  }

  onPreviousClick(): void {
    this.pokemonService.prev();
  }

  onNextClick(): void {
    this.pokemonService.next();
  }
}
