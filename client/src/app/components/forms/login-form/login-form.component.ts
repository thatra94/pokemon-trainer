import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PokemonService } from 'src/app/services/pokemon.service';
import { LoginService } from 'src/app/services/login.service';
import { RegisterService } from 'src/app/services/register.service';
import { getStorage } from 'src/app/utils/storage.utils';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css'],
})
export class LoginFormComponent implements OnInit {
  @Output() success: EventEmitter<void> = new EventEmitter();

  loginForm: FormGroup = new FormGroup({
    trainerName: new FormControl('', [
      Validators.required,
      Validators.minLength(2),
    ]),
  });

  constructor(
    private router: Router,
    private PokemonService: PokemonService,
    private readonly loginService: LoginService,
    private registerService: RegisterService
  ) {}

  get trainerName() {
    return this.loginForm.get('trainerName');
  }

  get loading(): boolean {
    return this.loginService.loading;
  }

  get error(): string {
    return this.loginService.error;
  }

  get errorCount(): number {
    return this.loginService.errorCount;
  }

  onLoginClicked() {
    console.log(this.loginForm.value);

    const { trainerName } = this.loginForm.value;

    this.loginService.login(trainerName).subscribe((res) => {
      this.handleSuccessfulLogin.bind(this);
      this.handleErrorLogin.bind(this);
      this.success.emit();
      this.router.navigateByUrl('/pokemon');
    });
  }

  handleSuccessfulLogin(trainer): void {
    this.success.emit();
  }

  handleErrorLogin(error): void {
    console.log(error);
    console.log(this.errorCount);
  }

  ngOnInit(): void {
    const existingTrainer: any = getStorage('pk-tr');
    if (existingTrainer != null) {
      this.loginForm.patchValue({
        trainerName: existingTrainer.name,
      });
      this.router.navigateByUrl('/pokemon');
    }
  }
}
