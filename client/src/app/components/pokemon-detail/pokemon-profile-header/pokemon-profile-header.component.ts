import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { TrainerPokemonService } from 'src/app/services/trainer-pokemon.service';
@Component({
  selector: 'app-pokemon-profile-header',
  templateUrl: './pokemon-profile-header.component.html',
  styleUrls: ['./pokemon-profile-header.component.css'],
})
export class PokemonProfileHeaderComponent implements OnInit {
  constructor(private readonly trainerPokemonService: TrainerPokemonService) {}

  @Input() pokemon: Pokemon;
  ngOnInit(): void {
    console.log(this.pokemon);
  }

  onCollectPokemon() {
    // read storage key or session stuff
    // add ability to fetch Pokemon object from server
    // add pokemon into list

    this.trainerPokemonService.addPokemon(this.pokemon);
    console.log(this.pokemon);
    console.log('Hello');
  }
}
