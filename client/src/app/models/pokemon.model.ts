export interface Pokemon {
  //Fill out with necesarry properties
  id?: number;
  name: string;
  url?: string;
  image?: string;
  weight?: number;
  height?: number;
  base_experience?: number;
  abilities?: PokemonAbility[];
  types?: PokmonType[];
  stats?: PokemonStat[];
  sprites?: PokemonSprite;
  moves?: PokemonMove[];
}

export interface PokemonAbility {
  ability: PokemonAbilityAbility;
}

export interface PokemonAbilityAbility {
  name: string;
  url: string;
}

export interface PokemonSprite {
  back_shiny: string;
  front_shiny: string;
  other: PokemonSpriteOther;
}

export interface PokemonSpriteOther {
  dream_world: PokemonSpriteDreamWorld;
  'official-artwork': PokemonSpriteOfficial;
}

export interface PokemonSpriteOfficial {
  front_default: string;
}

export interface PokemonSpriteDreamWorld {
  front_default: string;
}

export interface PokmonType {
  slot: number;
  type: PokemonTypeType;
}

export interface PokemonTypeType {
  name: string;
  url: string;
}

export interface PokemonStat {
  base_stat: number;
  effort: number;
  stat: PokemonStatStat;
}

export interface PokemonStatStat {
  name: string;
  url: string;
}

export interface PokemonMove {
  move: PokemonMoveMove;
}

export interface PokemonMoveMove {
  name: string;
  url: string;
}
