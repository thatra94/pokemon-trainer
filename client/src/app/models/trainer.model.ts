import { Pokemon } from "./pokemon.model";

export interface Trainer {
    //Fill out with necesarry properties
    id: number;
    name: string;
    pokemon: Pokemon[];
  }
  