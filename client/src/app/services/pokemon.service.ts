import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { PokemonResponse } from '../models/pokemon-response.model';
import { Pokemon } from 'src/app/models/pokemon.model';
import { environment } from 'src/environments/environment';
import { map, shareReplay } from 'rxjs/operators';
import { getMasterImageUrl } from '../utils/pokemon-image.util';
const { pokeAPI } = environment;

@Injectable({
  providedIn: 'root',
})
export class PokemonService {
  private readonly pokemonCache$;
  private _pokemon: Pokemon[] = [];
  public error: string = '';

  private offset = 0;
  private limit = 20;
  private count = 1;
  public isLastPage: boolean = false;
  public isFirstPage: boolean = true;

  constructor(private http: HttpClient) {
    this.pokemonCache$ = this.http
      .get<PokemonResponse>(`${pokeAPI}/pokemon?limit=100`)
      .pipe(shareReplay(1));
  }
  get pokemon(): Pokemon[] {
    //this.paginator.offsetStart, this.paginator.offsetEnd;
    return this._pokemon.slice(this.offset, this.offset + this.limit);
  }

  public next(): void {
    const nextOffset = this.offset + this.limit;
    if (nextOffset <= this.count - 1) {
      this.offset += this.limit;
    }
    this.updatePageStatus();
  }

  public prev(): void {
    if (this.offset !== 0) {
      this.offset -= this.limit;
    }
    this.updatePageStatus();
  }

  private updatePageStatus(): void {
    this.isFirstPage = this.offset === 0;
    this.isLastPage = this.offset === this.count - this.limit;
  }

  public fetchPokemon(): void {
    this.pokemonCache$
      .pipe(
        map((response: PokemonResponse) => {
          return response.results.map((pokemon: Pokemon) => ({
            ...pokemon,
            ...this.getIdAndImage(pokemon.url),
          }));
        })
      )
      .subscribe(
        (pokemon: Pokemon[]) => {
          this._pokemon = pokemon;
          this.count = pokemon.length;
        },
        (errorResponse: HttpErrorResponse) => {
          this.error = errorResponse.message;
        }
      );
  }

  private getIdAndImage(url: string): any {
    const id = Number(url.split('/').filter(Boolean).pop());
    return {
      id,
      url: getMasterImageUrl(id),
    };
  }
}
