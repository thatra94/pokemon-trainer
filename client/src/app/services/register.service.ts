import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { catchError, map } from 'rxjs/operators';
import { Trainer } from 'src/app/models/trainer.model';
import { Pokemon } from '../models/pokemon.model';
import { setStorage } from 'src/app/utils/storage.utils';
import { throwError } from 'rxjs';
const { pokeAPI, serverAPI } = environment;
import { BehaviorSubject, Observable } from 'rxjs';
import { analyzeAndValidateNgModules } from '@angular/compiler';

@Injectable({
  providedIn: 'root',
})
export class RegisterService {

  public loading = false;
  public user;

  constructor(private readonly http: HttpClient) { }
  private setTrainer(trainer: any) {
    setStorage('pk-tr', trainer);
  }

  public checkUser(name: string) {

    const currentUser = this.http.get(`${serverAPI}/trainers?name=${name}`)
      .pipe(
        map((response: any[]) => {
          return response
        }),
        catchError((error) => {
          return throwError(error);
        })
      )
    return currentUser
  }

  register(name: string): Observable<any> {

    this.checkUser(name)
      .subscribe(res => {
        console.log(res);
        
        if (res === []) {
          
          return this.http.post(`${serverAPI}/trainers`, {
            name,
            pokemons: []
          }).pipe(
            map((trainer) => {
              this.setTrainer(trainer);
              return trainer;
            })
          )
        } 
      }
      );
      return

  }

}