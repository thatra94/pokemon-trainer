import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import {
  catchError,
  finalize,
  flatMap,
  map,
  mergeMap,
  tap,
} from 'rxjs/operators';
import { Trainer } from 'src/app/models/trainer.model';
import { Pokemon } from '../models/pokemon.model';
import { setStorage, getStorage } from 'src/app/utils/storage.utils';

const { pokeAPI, serverAPI } = environment;

@Injectable({
  providedIn: 'root',
})
export class TrainerPokemonService {
  public currentPokemons = [];
  private userId = 1;
  public loading = false;
  public error: string = '';
  public errorCount: number = 0;

  constructor(private readonly http: HttpClient) {}

  public addPokemon(pokemon: Pokemon): Observable<any> {
    const currentUser = getStorage('pk-tr');
    console.log(pokemon.image);

    this.http
      .get<Trainer[]>(`${serverAPI}/trainers?name=${currentUser['name']}`)
      .subscribe((response) => {
        const trainer = response.pop() as Trainer;

        this.currentPokemons = trainer.pokemon;
        this.userId = trainer.id;

        const currentPokemonNames = trainer.pokemon.map(e => {
            return e.name;
        })

        const pokemonObject = {
          name: pokemon.name,
          url: pokemon.image,
        };
        const newPokemon = [...this.currentPokemons, pokemonObject];
        if (!currentPokemonNames.includes(pokemonObject.name)) {
          this.http
            .patch(`${serverAPI}/trainers/${this.userId}`, {
              pokemon: newPokemon,
            })
            .subscribe();
        }
      });
    return;
  }
}
