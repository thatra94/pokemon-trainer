import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Pokemon } from '../models/pokemon.model';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { getMasterImageUrl } from '../utils/pokemon-image.util';
const { pokeAPI } = environment;

@Injectable({
  providedIn: 'root',
})
export class PokemonDetailService {
  public pokemon: Pokemon;

  constructor(private readonly http: HttpClient) {}

  public fetchPokemonByName(name: string): void {
    this.http
      .get<Pokemon>(`${pokeAPI}/pokemon/${name}`)
      .pipe(
        map((pokemon: Pokemon) => ({
          ...pokemon,
          image: getMasterImageUrl(pokemon.id),
        }))
      )
      .subscribe((pokemon: Pokemon) => {
        this.pokemon = pokemon;
      });
  }
}
