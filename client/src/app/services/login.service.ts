import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { catchError, finalize, map, mergeMap, tap } from 'rxjs/operators';
import { Trainer } from 'src/app/models/trainer.model';
import { Pokemon } from '../models/pokemon.model';
import { setStorage } from 'src/app/utils/storage.utils';
import { StorageKeys } from '../enums/storage-keys.enum';

const { pokeAPI, serverAPI } = environment;

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  public loading = false;
  public error: string = '';
  public errorCount: number = 0;

  constructor(private readonly http: HttpClient) {}
  private setTrainer(trainer: any) {
    setStorage(StorageKeys.TRAINER, trainer);
  }

  public login(name: string): Observable<any> {
    const login$ = this.http.get<Trainer[]>(
      `${serverAPI}/trainers?name=${name}`
    );
    const register$ = this.http.post(`${serverAPI}/trainers`, {
      name: name,
      pokemon: [],
    });

    return login$.pipe(
      mergeMap((loginResponse: any[]) => {
        const trainer = loginResponse.pop() as Trainer;

        if (!trainer) {
          return register$;
        } else {
          delete trainer.id;
          return of(trainer);
        }
      }),
      tap((trainer: any) => {
        this.setTrainer(trainer);
      }),
      finalize(() => {
        this.loading = false;
      })
    );
  }
}
