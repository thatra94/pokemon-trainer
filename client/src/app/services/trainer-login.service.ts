import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Trainer } from 'src/app/models/trainer.model';
import { Pokemon } from '../models/pokemon.model';
const { pokeAPI ,serverAPI} = environment;
import { setStorage } from 'src/app/utils/storage.utils';


@Injectable({
  providedIn: 'root',
})
export class TrainerService {
  // OBservable
  private readonly _trainer$: BehaviorSubject<Trainer[]> = new BehaviorSubject(
    []
  );

  constructor(private http: HttpClient) { }

  public trainer$(): Observable<Trainer[]> {
    return this._trainer$.asObservable();
  }

  private setTrainer(trainer: any) {
    setStorage('pk-tr', trainer)
  }

  public fetchTrainers(): void {
    console.log("fetchTrainers: ", `${pokeAPI}/trainers`);

    this.http
      .get<Trainer>(`${serverAPI}/trainers`)
      .pipe(map((response: Trainer) => response.results))
      .subscribe(
        (trainer: Trainer[]) => {
          console.log(trainer);
          console.log(this._trainer$);

          this._trainer$.next(trainer);
        },
        (error) => {
          console.log('TrainerService.error', error);
        }
      );
  }
  
  public login() {
    // fetchTrainerByName
    // stuff
  }

  public fetchTrainerByName(name: string): Observable<any> {

    return this.http.get(`${serverAPI}/trainers?name=${name}`)
      .pipe(map((response: any) => response.results))
      
  }
}
