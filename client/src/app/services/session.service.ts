import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enum';
import { getStorage } from '../utils/storage.utils';

@Injectable({
  providedIn: 'root',
})
export class SessionService {
  active(): boolean {
    const trainer = getStorage(StorageKeys.TRAINER);
    return Boolean(trainer);
  }
}
