import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Pokemon } from 'src/app/models/pokemon.model';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { getStorage } from '../utils/storage.utils';


const { serverAPI } = environment;

@Injectable({
  providedIn: 'root',
})
export class TrainerService {

  private _pokemon: Pokemon[] = [];
  public error: string = '';

  constructor(private http: HttpClient) {

  }

  get pokemon(): Pokemon[] {
    return this._pokemon
  }

  public fetchUserPokemon() {
    const user = getStorage('pk-tr');

    this.http
      .get<any>(`${serverAPI}/trainers?name=${user['name']}`)
      .pipe(map(res => {
        return res.pop()['pokemon']
      }))
      .subscribe(
        (pokemon) => {
          console.log("in subscribe: ",pokemon);
          this._pokemon = pokemon;
          console.log(this._pokemon);
        },
        (errorResponse: HttpErrorResponse) => {
          this.error = errorResponse.message;
        }
      );
  }

}
