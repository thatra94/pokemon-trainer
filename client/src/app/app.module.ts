import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { ContainerComponent } from './components/container/container.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { AppComponent } from './app.component';
import { LoginFormComponent } from './components/forms/login-form/login-form.component';
import { PokemonComponent } from './components/pokemon/pokemon.component';
import { TrainerPageComponent } from './pages/trainer-page/trainer-page.component';
import { HttpClientModule } from '@angular/common/http';
import { CataloguePageComponent } from './pages/catalogue-page/catalogue-page.component';
import { PokemonDetailPageComponent } from './pages/pokemon-detail-page/pokemon-detail-page.component';
import { PokemonProfileHeaderComponent } from './components/pokemon-detail/pokemon-profile-header/pokemon-profile-header.component';
import { PokemonPaginationComponent } from './components/pokemon-pagination/pokemon-pagination.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { NotFoundContainerComponent } from './pages/not-found-page/not-found-page.component';

@NgModule({
  declarations: [
    AppComponent,
    ContainerComponent,
    LoginPageComponent,
    LoginFormComponent,
    TrainerPageComponent,
    CataloguePageComponent,
    PokemonDetailPageComponent,
    PokemonProfileHeaderComponent,
    PokemonPaginationComponent,
    PokemonComponent,
    NavbarComponent,
    NotFoundContainerComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
