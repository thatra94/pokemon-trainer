import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Injectable } from '@angular/core';

export interface Pagination {
  pages: number;
  currentPage: number;
  offsetStart: number;
  offsetEnd: number;
  itemCount: number;
  limit: number;
  isFirstPage: boolean;
  isLastPage: boolean;
}

export class PaginationUtility {
  pagination: Pagination = {
    pages: 0,
    limit: 0,
    itemCount: 0,
    currentPage: 1,
    isLastPage: false,
    isFirstPage: true,
    offsetEnd: 0,
    offsetStart: 0,
  };
  constructor(itemCount = 0, limit = 20) {
    this.pagination = {
      ...this.pagination,
      offsetEnd: this.pagination.offsetStart + limit,

      itemCount,
      limit,
      pages: Math.floor(itemCount / limit),
    };
  }

  getPagination(): Pagination {
    return this.pagination;
  }

  next(): void {
    const nextOffset = this.pagination.offsetStart + this.pagination.limit;
    if (nextOffset < this.pagination.itemCount - 1) {
      return;
    }
    this.pagination = {
      ...this.pagination,
      offsetStart: nextOffset,
      currentPage: this.calculateCurrentPage(nextOffset),
      isFirstPage: nextOffset === 0,
      isLastPage:
        nextOffset === this.pagination.itemCount - this.pagination.limit,
    };
  }

  prev(): void {
    if (this.pagination.offsetStart === 0) {
      return;
    }

    const nextOffset = this.pagination.offsetStart - this.pagination.limit;
    this.pagination = {
      ...this.pagination,
      offsetStart: nextOffset,
      currentPage: this.calculateCurrentPage(nextOffset),
      isFirstPage: nextOffset === 0,
      isLastPage:
        nextOffset === this.pagination.itemCount - this.pagination.limit,
    };
  }

  last(): void {
    const lastOffset = this.pagination.itemCount - this.pagination.limit;
    this.pagination = {
      ...this.pagination,
      offsetStart: lastOffset,
      offsetEnd: this.pagination.offsetStart + this.pagination.limit,
      currentPage: this.calculateCurrentPage(lastOffset),
      isFirstPage: false,
      isLastPage: true,
    };
  }

  first(): void {
    this.pagination = {
      ...this.pagination,
      offsetStart: 0,
      currentPage: 1,
      isFirstPage: true,
      isLastPage: false,
      offsetEnd: this.pagination.offsetStart + this.pagination.limit,
    };
  }
  private calculateCurrentPage(offset: number): number {
    return Math.ceil(offset / this.pagination.limit) + 1;
  }
}
